import busio
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
import RPi.GPIO as GPIO
import time
import threading
# create the spi bus
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
rate = 5 # Default Sampling rate 
runTime = time.perf_counter() # current timer
# create the cs (chip select)
cs = digitalio.DigitalInOut(board.D5)

# create the mcp object
mcp = MCP.MCP3008(spi, cs)

# create an analog input channel on pin 0 and pin 1
Lchan = AnalogIn(mcp, MCP.P0) #LDR Channel
Tchan = AnalogIn(mcp, MCP.P1) #Temparature Channel

def setup():
    btn = 3
    GPIO.setup(btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(btn, GPIO.RISING, callback=change_sampling_rate,bouncetime=300)
    
def change_sampling_rate(channel):
    global rate
    if (rate == 10):
        rate = 5
    elif (rate == 5):
        rate = 1
    elif (rate == 1):
        rate = 10
    print("\n Rate to changed to" , str(rate) , "s\n")

def report():
    global Tchan
    global Lchan
    global rate
    global runTime
    thread = threading.Timer(rate, report)
    thread.daemon = True  # Daemon threads exit when the program does
    thread.start()
    V = Tchan.voltage
    T = (V-0.5)/0.01
    T = "{:.2f}".format(T)+"C"
    timeC = str(int(time.perf_counter() - runTime))+"s"
    print("{:<9} {:<15} {:<9} {:<10}".format(timeC,Tchan.value,T,Lchan.value))


def main():
    global rate
    print("Welcome to light and temparature sensor reading")
    print("Setting up GIOP button")
    setup()
    print("Press button to change sampling rate and press Q to quit")
    print("Runtime  Temp Reading    Temp      Light Reading")
    report()
    option = "S"
    while True:
        option = input()
        if (option == "Q"):
            print("Thank You Bye!!")
            exit(-1)
        pass


if __name__ == "__main__":
    main()
